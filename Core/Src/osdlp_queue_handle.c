/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "osdlp_queue_handle.h"
#include "osdlp_config.h"
#include "radio.h"
#include "qubik.h"
#include <cmsis_os.h>
#include <string.h>

extern osMutexId osdlp_rx_mtxHandle;
extern bool last_pkt_avail;
extern struct qubik hqubik;

struct rx_bundle rx_bun;

static struct tx_bundle tx_bun;
static struct tx_frame_metadata metadata;
static struct tc_transfer_frame *last_tc;

int
initialize_osdlp()
{
	tm_master_channel_count = 0;
	int ret;
	for (int i = 0 ; i < OSDLP_TC_VCS; i++) {
		ret = init_queue(&rx_queues[i],
		                 sizeof(struct rx_bundle),
		                 OSDLP_TC_ITEMS_PER_QUEUE,
		                 tc_mem_space[i]);
		if (rx_queues[i].is_init != NO_ERROR) {
			return -OSDLP_INIT_ERROR;
		}
		osdlp_prepare_farm(&tc_configs[i].cop_cfg.farm, FARM_STATE_OPEN,
		                   tc_window_width[i]);
		ret = osdlp_tc_init(&tc_configs[i], hqubik.settings.scid,
		                    OSDLP_MAX_TC_PACKET_LENGTH,
		                    OSDLP_MAX_TC_FRAME_SIZE, OSDLP_TC_ITEMS_PER_QUEUE, i, 0, tc_crc_on[i],
		                    tc_segmentation_per_vcid[i], 0, 0, tc_util_buffer[i], tc_configs[i].cop_cfg);
		if (ret == -1) {
			return -OSDLP_INIT_ERROR;
		}
	}
	for (int i = 0 ; i < OSDLP_TM_VCS; i++) {
		ret = init_queue(&tx_queues[i],
		                 sizeof(struct tx_bundle),
		                 OSDLP_TM_ITEMS_PER_QUEUE,
		                 tm_mem_space[i]);
		if (tx_queues[i].is_init != NO_ERROR) {
			return -OSDLP_INIT_ERROR;
		}
		ret = osdlp_tm_init(&tm_configs[i], hqubik.settings.scid,
		                    &tm_master_channel_count, i,
		                    tm_ocf_flag[i], tm_ocf_type[i], tm_sec_hdr_on[i],
		                    tm_sync_flag[i], tm_sec_hdr_len[i],
		                    NULL, tm_crc_on[i],
		                    OSDLP_TM_FRAME_SIZE, OSDLP_MAX_TM_PACKET_LENGTH,
		                    OSDLP_TM_VCS,
		                    OSDLP_TM_ITEMS_PER_QUEUE,
		                    tm_stuffing_state[i],
		                    tm_util_buffer[i]);
		if (ret == -1) {
			return -OSDLP_INIT_ERROR;
		}
	}
	osdlp_state.tc_configs = tc_configs;
	osdlp_state.tm_configs = tm_configs;
	osdlp_state.rx_queues  = rx_queues;
	osdlp_state.tx_queues  = tx_queues;
	return NO_ERROR;
}

struct osdlp_state_t *
get_osdlp_state()
{
	return &osdlp_state;
}

bool
osdlp_tc_rx_queue_full(uint16_t vcid)
{
	if (vcid >= OSDLP_TC_VCS)
		return true;
	return rx_queues[vcid].inqueue ==
	       rx_queues[vcid].capacity ? true : false;
}

int
osdlp_tc_rx_queue_enqueue(uint8_t *buffer, uint32_t length, uint16_t vcid)
{
	int ret;
	if (vcid >= OSDLP_TC_VCS)
		return -INVAL_PARAM;
	if (osMutexWait(osdlp_rx_mtxHandle, 50) == osOK) {
		memcpy(rx_bun.frame, buffer, length * sizeof(uint8_t));
		rx_bun.mapid = tc_configs[vcid].frame_data.seg_hdr.map_id;
		rx_bun.frame_len = length;
		ret = enqueue(&rx_queues[vcid], &rx_bun, sizeof(struct rx_bundle));
		osMutexRelease(osdlp_rx_mtxHandle);
		return ret;
	} else {
		return -OSDLP_MTX_LOCK;
	}
}

int
osdlp_tc_rx_queue_enqueue_now(uint8_t *buffer, uint32_t length, uint8_t vcid)
{
	int ret;
	if (vcid >= OSDLP_TC_VCS)
		return -INVAL_PARAM;
	if (osMutexWait(osdlp_rx_mtxHandle, 50) == osOK) {
		memcpy(rx_bun.frame, buffer, length * sizeof(uint8_t));
		rx_bun.mapid = tc_configs[vcid].frame_data.seg_hdr.map_id;
		rx_bun.frame_len = (uint16_t)length;
		ret = enqueue(&rx_queues[vcid], &rx_bun, sizeof(struct rx_bundle));
		osMutexRelease(osdlp_rx_mtxHandle);
	} else {
		return -OSDLP_MTX_LOCK;
	}
	if (ret < 0) {
		if (osdlp_tc_rx_queue_full(vcid)) {
			if (osMutexWait(osdlp_rx_mtxHandle, 50) == osOK) {
				ret = enqueue_now(&rx_queues[vcid], &rx_bun, sizeof(struct rx_bundle));
				osMutexRelease(osdlp_rx_mtxHandle);
				return NO_ERROR;
			} else {
				return -OSDLP_MTX_LOCK;
			}
		} else {
			return -OSDLP_QUEUE_FULL;
		}
	} else {
		return NO_ERROR;
	}
}

int
osdlp_tc_get_rx_config(struct tc_transfer_frame **tf, uint16_t vcid)
{
	if (vcid >= OSDLP_TC_VCS || vcid == 4) {
		return -INVAL_PARAM;
	}
	*tf = &tc_configs[vcid];
	if (tf != NULL) {
		last_tc = &tc_configs[vcid];
		return NO_ERROR;
	} else {
		return -OSDLP_NULL;
	}
}

int
tm_get_tx_config(struct tm_transfer_frame **tf, uint16_t vcid)
{
	if (vcid >= OSDLP_TM_VCS)
		return -INVAL_PARAM;
	*tf = &tm_configs[vcid];
	return NO_ERROR;
}

bool
osdlp_tm_tx_queue_empty(uint8_t vcid)
{
	if (vcid >= OSDLP_TM_VCS)
		return true;
	return (tx_queues[vcid].inqueue == 0);
}

int
osdlp_tm_tx_queue_back(uint8_t **pkt, uint8_t vcid)
{
	int ret;
	if (vcid >= OSDLP_TM_VCS)
		return -1;
	last_pkt_avail = false;
	ret = back(&tx_queues[vcid], *pkt);
	if (ret < 0) {
		last_pkt_avail = true;
		return ret;
	} else {
		return NO_ERROR;
	}
}

int
osdlp_tm_tx_queue_enqueue(uint8_t *pkt, uint8_t vcid)
{
	int ret;
	if (vcid >= OSDLP_TM_VCS) {
		return -INVAL_PARAM;
	}
	memcpy(tx_bun.frame, pkt, OSDLP_TM_FRAME_SIZE);
	tx_bun.meta = metadata;
	ret = enqueue(&tx_queues[vcid], &tx_bun, sizeof(struct tx_bundle));
	return ret;
}

void
osdlp_tm_tx_commit_back(uint8_t vcid)
{
	last_pkt_avail = true;
	return;
}

struct tc_transfer_frame *
get_last_tc()
{
	return last_tc;
}

tm_stuff_state_t
stuffing_on(uint8_t vcid)
{
	return tm_stuffing_state[vcid];
}

int
register_meta(const struct tx_frame_metadata *meta)
{
	memcpy(&metadata, meta, sizeof(struct tx_frame_metadata));
	return NO_ERROR;
}
