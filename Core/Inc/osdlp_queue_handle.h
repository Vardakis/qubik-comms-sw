/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INC_OSDLP_QUEUE_HANDLE_H_
#define INC_OSDLP_QUEUE_HANDLE_H_

#include <Drivers/OSDLP/include/osdlp.h>
#include "queue_util.h"
#include "conf.h"
#include "radio.h"
#include <stddef.h>
#include "error.h"

struct tx_bundle {
	uint8_t frame[OSDLP_TM_FRAME_SIZE];
	struct tx_frame_metadata meta;
};

struct rx_bundle {
	uint8_t frame[OSDLP_MAX_TC_FRAME_SIZE];
	uint8_t mapid;
	uint16_t frame_len;
};

uint8_t tc_mem_space[OSDLP_TC_VCS][OSDLP_TC_ITEMS_PER_QUEUE *
                                   sizeof(struct tx_bundle)];
uint8_t tm_mem_space[OSDLP_TM_VCS][OSDLP_TM_ITEMS_PER_QUEUE *
                                   OSDLP_TM_FRAME_SIZE];
uint8_t tc_util_buffer[OSDLP_TC_VCS][OSDLP_MAX_TC_PACKET_LENGTH];
uint8_t tm_util_buffer[OSDLP_TM_VCS][OSDLP_TM_FRAME_SIZE];

struct tc_transfer_frame tc_configs[OSDLP_TC_VCS];
struct tm_transfer_frame tm_configs[OSDLP_TM_VCS];

struct queue rx_queues[OSDLP_TC_VCS];
struct queue tx_queues[OSDLP_TM_VCS];

uint8_t tm_master_channel_count;

struct osdlp_state_t {
	struct queue *rx_queues;
	struct queue *tx_queues;
	struct tc_transfer_frame *tc_configs;
	struct tm_transfer_frame *tm_configs;
};

int
initialize_osdlp();

struct osdlp_state_t *
get_osdlp_state();

struct tc_transfer_frame *
get_last_tc();

int
tm_get_tx_config(struct tm_transfer_frame **tf, uint16_t vcid);

tm_stuff_state_t
stuffing_on(uint8_t vcid);

struct osdlp_state_t osdlp_state;

int
register_meta(const struct tx_frame_metadata *meta);


#endif /* INC_OSDLP_QUEUE_HANDLE_H_ */
