/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INC_MAX17261_DRIVER_H_
#define INC_MAX17261_DRIVER_H_

#include <Drivers/MAX17261/include/max17261.h>
/** Battery capacity in mAh */
#define	BATTERY_CAPACITY	2480
/** Charge termination current in mA */
#define BATTERY_CRG_TERM_I	25
#define BATTERY_V_EMPTY		(3300 / 10)
#define BATTERY_V_Recovery	(3880 / 40)

/** Initial model parameters */
#ifndef MAX17261_RCOMP0_VAL
#define MAX17261_RCOMP0_VAL		0
#endif
#ifndef MAX17261_TEMP_CO_VAL
#define MAX17261_TEMP_CO_VAL	0
#endif
#ifndef MAX17261_QRTABLE01_VAL
#define MAX17261_QRTABLE01_VAL	0
#endif
#ifndef MAX17261_QRTABLE02_VAL
#define MAX17261_QRTABLE02_VAL	0
#endif
#ifndef MAX17261_QRTABLE03_VAL
#define MAX17261_QRTABLE03_VAL	0
#endif
#ifndef MAX17261_QRTABLE04_VAL
#define MAX17261_QRTABLE04_VAL	0
#endif

// Set init option
#if MAX17261_RCOMP0_VAL != 0 && MAX17261_TEMP_CO_VAL != 0 && MAX17261_QRTABLE01_VAL != 0 && MAX17261_QRTABLE02_VAL != 0 && MAX17261_QRTABLE03_VAL != 0 && MAX17261_QRTABLE04_VAL != 0
#define MAX17261_INIT_OPTION 2
#pragma message("MAX17261 init option 2")
#else
#define MAX17261_INIT_OPTION 1
#pragma message("MAX17261 init option 1")
#endif

#endif /* INC_MAX17261_DRIVER_H_ */
