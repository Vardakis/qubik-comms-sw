/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POWER_H_
#define POWER_H_

#include "max17261_driver.h"
#include "main.h"

#define ADC_VREF						3300.0
#define	POWER_V_ALT_COEFF				1.407

#define POWER_LOW_ENTRY_V_THRESHOLD		3450
#define POWER_LOW_EXIT_V_THRESHOLD		3800
#define POWER_LOW_ENTRY_SOC_THRESHOLD	20
#define POWER_LOW_EXIT_SOC_THRESHOLD	25

#define POWER_CHG_VOLTAGE				4.35
/** Brown out voltage */
#define POWER_BO_VOLTAGE				1600

#define POWER_CH_V_ALT					(ADC_CHANNEL_8)
#define POWER_CH_PDET					(ADC_CHANNEL_5)
#define POWER_CH_V_RTC					(ADC_CHANNEL_VBAT)
#define POWER_CH_TEMP					(ADC_CHANNEL_TEMPSENSOR)
/** Power polling interval in ms */
#define	POWER_POLLING_INTERVAL			1000

/**
 * @brief Power status structure
 *
 * Holds power related values
 */
struct power_status {
	uint16_t voltage; 			/**< Voltage in mV */
	uint16_t voltage_alt; 		/**< Voltage in mV sensed from STM32*/
	uint16_t voltage_avg; 		/**< Average Voltage in mV */
	uint16_t voltage_max; 		/**< Max Voltage in mV */
	uint16_t voltage_min; 		/**< Min Voltage in mV */
	int16_t current; 			/**< Current in mA */
	int16_t current_avg; 		/**< Average Current in mA */
	int16_t current_max; 		/**< Max in mA */
	int16_t current_min; 		/**< Min in mA */
	int8_t temperature_die; 	/**< Current in mA */
	int8_t temperature; 		/**< Current in mA */
	int8_t temperature_avg; 	/**< Average Current in mA */
	int8_t temperature_max; 	/**< Max in mA */
	int8_t temperature_min; 	/**< Min in mA */
	int8_t SOC; 				/**< State of charge in % */
	int16_t FullRepCap;			/**< new full-capacity value  */
	int16_t RepCap;				/**< reported remaining capacity in mAh. */
	uint16_t TTE; 				/**< Time to empty in minutes */
	uint32_t pa_output_power; 	/**< PA output power */
	uint16_t rtc_vbat; 			/**< RTC Battery Voltage in mV */
	int8_t temperature_mcu; 	/**< MCU temperature */
};

/**
 * @brief QUBIK power state values
 */
typedef enum {
	POWER_STATE_OK,     	//!< POWER_STATE_OK
	POWER_STATE_DEGRADED	//!< POWER_STATE_DEGRADED
} power_state_t;

int
update_power_status(struct power_status *power_status);

void
power_task();

#endif /* POWER_H_ */
